package ObjectRepository;

import Init.BaseClass;
import Utils.Utility;
import org.apache.commons.validator.GenericValidator;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.Reporter;

import java.awt.*;
import java.time.Month;
import java.time.LocalDate;
import java.util.*;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static java.awt.SystemColor.window;
import static org.testng.Assert.*;

public class RegisterPage extends BaseClass {

    public RegisterPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "xpath of the otp filed")
    WebElement OtpFiled;

    @FindBy(xpath = "xpath of the otp")
    WebElement Otp;

    @FindBy(xpath = "xpath of the otp placeholder")
    WebElement OtpPlaceholder;

    @FindBy(xpath = "xpath of the timer placeholder")
    WebElement Timer;

    @FindBy(xpath = "xpath of the resend code text")
    WebElement ResendCodeButton;

    @FindBy(xpath = "xpath of the address filed text")
    WebElement AddressFiled;

    @FindBy(xpath = "xpath of the address placeholder text")
    WebElement AddressPlaceholder;

    @FindBy(xpath = "xpath of address")
    WebElement Address;

    @FindBy(xpath = "xpath of address error filed")
    WebElement AddressErrorFiled;

    @FindBy(xpath = "xpath of the gender filed text")
    WebElement GenderFiled;

    @FindBy(xpath = "xpath of the dropdown")
    WebElement DropDown;

    @FindBy(xpath = "xpath of the dropdown value")
    WebElement DropDownValue;

    @FindBy(xpath = "xpath of the dropdown arrow button")
    WebElement DropDownArrowButton;

    @FindBy(xpath = "xpath of the outside of the dropdown")
    WebElement DropDownOutside;

    @FindBy(xpath = "xpath of country code filed text")
    WebElement CountryCodeFiled;

    @FindBy(xpath = "xpath of country code placeholder ")
    WebElement CountryCode;

    @FindBy(xpath = "xpath of country code arrow button")
    WebElement CountryCodeArrowButton;

    @FindBy(xpath = "xpath of country code value")
    WebElement CountryCodeValue;

    @FindBy(xpath = "xpath of country code search")
    WebElement CountryCodeSearch;

    @FindBy(xpath = "xpath of the outside of the dropdown for country code")
    WebElement CountryCodeDropDownOutside;

    @FindBy(xpath = "xpath of country filed text")
    WebElement CountryFiled;

    @FindBy(xpath = "xpath of country placeholder ")
    WebElement Country;

    @FindBy(xpath = "xpath of country arrow button")
    WebElement CountryArrowButton;

    @FindBy(xpath = "xpath of country value")
    WebElement CountryValue;

    @FindBy(xpath = "xpath of the outside of the dropdown for country")
    WebElement CountryDropDownOutside;

    @FindBy(xpath = "xpath of state filed text")
    WebElement StateFiled;

    @FindBy(xpath = "xpath of state placeholder ")
    WebElement State;

    @FindBy(xpath = "xpath of state arrow button")
    WebElement StateArrowButton;

    @FindBy(xpath = "xpath of state value")
    WebElement StateValue;

    @FindBy(xpath = "xpath of the outside of the dropdown for country")
    WebElement StateDropDownOutside;

    @FindBy(xpath = "xpath of the search functionality in state dropdown")
    WebElement StateSearch;

    @FindBy(xpath = "xpath of city filed text")
    WebElement CityFiled;

    @FindBy(xpath = "xpath of city")
    WebElement City;

    @FindBy(xpath = "xpath of city Search ")
    WebElement CitySearch;

    @FindBy(xpath = "xpath of city arrow button")
    WebElement CityArrowButton;

    @FindBy(xpath = "xpath of city value")
    WebElement CityValue;

    @FindBy(xpath = "xpath of the outside of the dropdown for city")
    WebElement CityDropDownOutside;

    @FindBy(xpath = "xpath of the zip code")
    WebElement ZipCode;

    @FindBy(xpath = "xpath of the zip code filed")
    WebElement ZipCodeFiled;

    @FindBy(xpath = "xpath of the zip code placeholder")
    WebElement ZipCodePlaceHolder;

    @FindBy(xpath = "xpath of the zip code error")
    WebElement ZipCodeError;

    @FindBy(xpath = "xpath of the birthdate")
    WebElement BirthDate;

    @FindBy(xpath = "//label[text()='DOB']")
    WebElement BirthDateFiled;

    @FindBy(xpath = "xpath of the birthdate placeholder")
    WebElement BirthDatePlaceHolder;

    @FindBy(xpath = "//div[@class='rdrCalendarWrapper']")
    WebElement Calendar;

    @FindBy(xpath = "//button[@class='rdrDay rdrDayToday']")
    WebElement CalendarValue;

    @FindBy(xpath = "//span[@class='rdrMonthPicker']")
    WebElement CalendarMonth;

    @FindBy(xpath = "//img[@src='../calender.svg']")
    WebElement CalenderArrowButton;

    @FindBy(xpath = "//input[@class='mt-1 col-12 text-align-center loginInput']")
    WebElement CalenderInputBox;


    @FindBy(xpath = "//button[@class='rdrNextPrevButton rdrPprevButton']")
    WebElement CalenderPreviousButton;

    @FindBy(xpath = "//button[@class='rdrNextPrevButton rdrNextButton']")
    WebElement CalenderNextButton;

    @FindBy(xpath = "xpath of the phone number filed")
    WebElement PhoneNumberFiled;

    @FindBy(xpath = "xpath of the phone number place holder")
    WebElement PhoneNumberPlaceHolder;

    @FindBy(xpath = "xpath of the phone number")
    WebElement PhoneNumber;

    @FindBy(xpath = "xpath of the phone number error")
    WebElement PhoneNumberError;

    @FindBy(xpath = "xpath of the phone number keyboard")
    WebElement PhoneNumberKeyBoard;

    @FindBy(xpath = "xpath of the phone number arrow button")
    WebElement PhoneNumberArrowButton;

    @FindBy(xpath = "xpath of the phone number value")
    WebElement PhoneNumberValue;

    @FindBy(xpath = "xpath of the phone number drop down")
    WebElement PhoneNumberDropDownOutside;

    @FindBy(xpath = "xpath of the phone number outside")
    WebElement PhoneNumberOutside;



    //---verify otp filed text---//
    public void verify_otp_filed_name() {
        String OtpFiledText = OtpFiled.getAttribute("value");
        if (OtpFiledText.equals("OTP")) {
            test.info("Filed name matched");
        } else {
            test.info("Filed name does not matched");
        }

    }

    //---verify otp placeholder---//
    public void verify_name_placeholder() {
        String OtpPlaceHolderText = OtpPlaceholder.getAttribute("value");
        if (OtpPlaceHolderText.equals("0 0 0 0 0 0")) {
            test.info("Place holder name matched");
        } else {
            test.info("Place holder name does not matched");
        }
    }


    //--- verify otp timer ---//

    public void verify_timer() throws InterruptedException {
        String StartTime = Timer.getAttribute("value");
        Thread.sleep(3000);
        String EndTime = Timer.getAttribute("value");
        if (StartTime.equals(EndTime)) {
            test.info("Timer does not working");
        } else {
            test.info("Timer is working");
        }
    }


    //--- verify_timer_should_be_in_sec_min ---//
    public void verify_timer_should_be_in_sec_min() {
        if (Timer.getAttribute("value").contains("s") || Timer.getAttribute("value").contains("m")) {
            test.info("timer is in valid format");
        } else {
            test.info("timer does not in valid format");
        }
    }


    //--- verify resend code functionality ---//
    public void verify_resend_code_functionality() {
        String ResendCodeText = ResendCodeButton.getAttribute("value");
        Assert.assertEquals(ResendCodeText, "Resend");
    }

    //--- verify resend code button ---//
    public void verify_resend_code_button() throws InterruptedException {
        ResendCodeButton.click();
        Thread.sleep(1000);
        boolean Resendcode = ResendCodeButton.isEnabled();
        if (Resendcode == true) {
            test.info("Resend code does not working");
        } else {
            test.info("Resend code is working");
        }
    }

    public void verify_resend_code_button_should_be_enable_when_timer_is_off() throws InterruptedException {
        Otp.click();
        Thread.sleep(35000);
        ResendCodeButton.isEnabled();
        Otp.sendKeys(Keys.TAB);

    }

    //--- verify hand icon show hover over the eye icon for resend code link---//
    public void verify_hand_icon_hover_over_the_eye_icon_resend_code_link() throws InterruptedException {
        String handicon = ResendCodeButton.getCssValue("cursor");
        Assert.assertEquals("handicon", "pointer");
    }

    //---verify address filed text---//
    public void verify_address_filed_name() {
        String AddressFiledText = AddressFiled.getAttribute("value");
        if (AddressFiledText.equals("Address")) {
            test.info("Filed name matched");
        } else {
            test.info("Filed name does not matched");
        }

    }


    //---verify address placeholder---//
    public void verify_address_placeholder() {
        String AddressPlaceholderText = AddressPlaceholder.getAttribute("value");
        if (AddressPlaceholderText.equals("Address")) {
            test.info("Place holder name matched");
        } else {
            test.info("Place holder name does not matched");
        }
    }

    //--- verify address with more than 140 character ---//

    public void verify_address_with_more_than_140_character() {
        Address.sendKeys("kjakjdjhhqewewqoiewwoieoiuexmxmsakjoisqoiwjoieioksajklwjejieoweiorwiouwrioururiworiwoirwxss");
        String AddressErrorText = AddressErrorFiled.getAttribute("value");
        Assert.assertEquals(AddressErrorText, "Address must be maximum 140 characters long.");

    }

    //--- verify address with blank space ---//

    public void verify_address_with_blank_space() {
        Address.sendKeys("  TEST  ");
        String AddressText = Address.getAttribute("value");
        Assert.assertEquals(AddressText, "TEST");
        Address.click();
        Address.sendKeys(Keys.TAB);
    }


    //---verify gender filed text---//
    public void verify_gender_filed_name() {
        String GenderFiledText = GenderFiled.getAttribute("value");
        if (GenderFiledText.equals("Gender")) {
            test.info("Filed name matched");
        } else {
            test.info("Filed name does not matched");
        }
    }

    //---verify dropdown scrollable ---//
    public void verify_dropdown_scrollable() {
        for (int i = 0; i <= DropDownValue.getAttribute("value").length(); i++) {
            DropDown.sendKeys(Keys.ARROW_DOWN);
        }

    }

    public void verify_by_default_select_the_one_option(){
        DropDown.click();
        List<WebElement> genderlist = new ArrayList<>();
        for(WebElement testn :genderlist){
            if(testn.isSelected()){
                test.info("By Default one item is selected");
            }
            else{
                test.info("By default one item does not selected");
            }
        }
    }

    public void verify_by_default_one_radio_button_selected(){
        DropDown.click();
        List<WebElement> genderlist = new ArrayList<>();
        genderlist.get(1).click();
        Boolean twoelements = genderlist.get(0).isSelected();
        if(!twoelements == true){
            test.info("Only one element selected");
        }else{
            test.info("Two elements are selected");
        }

    }

    public void verify_first_entry(){
        DropDown.click();
        List<WebElement> genderlist = new ArrayList<>();
        String FirstEntryOfGender = genderlist.get(0).getAttribute("value");
        Assert.assertEquals(FirstEntryOfGender,"Please Select Gender");
    }


    public void verify_dropdown_value_is_selectable() {
        DropDown.click();
        DropDownValue.click();
        DropDown.isSelected();
        Assert.assertSame(DropDown.getAttribute("value"), DropDownValue.getAttribute("value"));

    }

    public void verify_dropdown_done_button() {
        DropDown.click();
        if (DropDown.getAttribute("value").equalsIgnoreCase("Done")) {
            test.info("Done Button is available");
        }
        if (DropDown.getAttribute("value").equalsIgnoreCase("Cancel")) {
            test.info("Cancel Button is available");
        } else {
            test.info("Done and cancel Button does not available");
        }
    }


    public void verify_dropdown_arrow_button() {
        DropDownArrowButton.isEnabled();
        DropDownArrowButton.click();
        if (DropDownValue.isDisplayed()) {
            test.info("DropDown is opened");
        } else {
            test.info("DropDown is closed");
        }
    }

    public void verify_when_click_outside_the_dropdown() {
        DropDown.click();
        if (DropDownValue.isDisplayed()) {
            DropDownOutside.click();
        }
        if (DropDownValue.isDisplayed()) {
            test.info("Dropdown is still opened");
        } else {
            test.info("Dropdown is opened");
        }
    }

    public void verify_tab_is_working() {
        DropDown.click();
        DropDown.sendKeys(Keys.TAB);
    }

    public void verify_when_hover_the_close_icon_it_should_show_hand_icon() {
        String handicon = DropDownArrowButton.getCssValue("cursor");
        Assert.assertEquals(handicon, "pointer");
    }

    public void verify_when_hover_the_radio_it_should_show_hand_icon() {
        String handicon = DropDownValue.getCssValue("cursor");
        Assert.assertEquals(handicon, "pointer");
    }

    //---verify country code filed text---//
    public void verify_country_code_filed_name() {
        String CountryCodeFiledText = CountryCodeFiled.getAttribute("value");
        if (CountryCodeFiledText.equals("Country Code")) {
            test.info("Filed name matched");
        } else {
            test.info("Filed name does not matched");
        }

    }


    //---verify country code placeholder---//
    public void verify_country_code_placeholder() {
        String CountryCodeText = CountryCode.getAttribute("value");
        if (CountryCodeText.equals("Country Code")) {
            test.info("Place holder name matched");
        } else {
            test.info("Place holder name does not matched");
        }
    }

    public void verify_first_entry_of_country_code(){
        CountryCode.click();
        List<WebElement> CountryCodelist = new ArrayList<>();
        String FirstEntryOfCountryCode = CountryCodelist.get(0).getAttribute("value");
        Assert.assertEquals(FirstEntryOfCountryCode,"Select Country Code");
    }


    public void verify_country_code_list_should_be_in_ascending(){
        CountryCode.click();
        Select se = new Select(CountryCode);
        List<String> originalList = new ArrayList();
        originalList.remove(0);
        for (WebElement e : se.getOptions()) {
            originalList.add(e.getAttribute("value"));
        }

        List<String> tempList= originalList;
        Collections.sort(tempList);
        Assert.assertEquals(tempList, originalList);
    }

    public void verify_search_functionality_should_be_there_for_country_code(){
        CountryCode.click();
        if(CountryCodeSearch.isDisplayed()==true){
            test.info("Search Functionality available for country code");
        }
        else{
            test.info("Search Functionality does not available for country code ");
        }
    }

    public void verify_it_show_selected_option_for_country_code(){
        CountryCode.click();
        List<WebElement> CountryCodelist = new ArrayList<>();
        String FirstEntryOfCountryCode = CountryCodelist.get(1).getAttribute("value");
        CountryCodelist.get(1).click();

        String CountryCodeText = CountryCode.getAttribute("value");
        Assert.assertEquals(CountryCodeText,FirstEntryOfCountryCode);

    }

    public void verify_country_code_accept_the_emoji() throws InterruptedException {
        CountryCode.click();
        CountryCode.sendKeys("\uD83D\uDE12");
        Thread.sleep(1000);
        String CountryCodeText = CountryCode.getAttribute("value");
        Assert.assertSame(CountryCodeText, "");
        CountryCode.sendKeys(Keys.TAB);
    }

    public void verify_dropdown_arrow_button_work_for_country_code() {
        CountryCodeArrowButton.isEnabled();
        CountryCodeArrowButton.click();
        if (CountryCodeValue.isDisplayed()) {
            test.info("DropDown is opened");
        } else {
            test.info("DropDown is closed");
        }
    }

    public void verify_when_click_outside_the_dropdown_for_country_code() {
        CountryCode.click();
        if (CountryCodeDropDownOutside.isDisplayed()) {
            DropDownOutside.click();
        }
        if (CountryCodeDropDownOutside.isDisplayed()) {
            test.info("Dropdown is still opened");
        } else {
            test.info("Dropdown is opened");
        }
    }

    //---verify country code dropdown scrollable ---//
    public void verify_country_code_dropdown_scrollable() {
        for (int i = 0; i <= CountryCodeValue.getAttribute("value").length(); i++) {
            CountryCode.sendKeys(Keys.ARROW_DOWN);
        }
    }

    public void verify_when_hover_the_close_icon_it_should_show_hand_icon_for_country_code() {
        String handicon = CountryCodeArrowButton.getCssValue("cursor");
        Assert.assertEquals(handicon, "pointer");
    }

    public void verify_country_code_in_ascending_order() {
        String country_code[] = new String[]{CountryCodeValue.getAttribute("value")};
        Arrays.sort(country_code);
    }


    //---verify country code filed text---//
    public void verify_country_filed_name() {
        String CountryFiledText = CountryFiled.getAttribute("value");
        if (CountryFiledText.equals("Country")) {
            test.info("Filed name matched");
        } else {
            test.info("Filed name does not matched");
        }

    }


    //---verify country code placeholder---//
    public void verify_country_placeholder() {
        String CountryText = Country.getAttribute("value");
        if (CountryText.equals("Country")) {
            test.info("Place holder name matched");
        } else {
            test.info("Place holder name does not matched");
        }
    }

    public void verify_country_accept_the_emoji() throws InterruptedException {
        Country.click();
        Country.sendKeys("\uD83D\uDE12");
        Thread.sleep(1000);
        String CountryText = CountryCode.getAttribute("value");
        Assert.assertSame(CountryText, "");
        Country.sendKeys(Keys.TAB);
    }

    public void verify_dropdown_arrow_button_work_for_country() {
        CountryArrowButton.isEnabled();
        CountryArrowButton.click();
        if (CountryValue.isDisplayed()) {
            test.info("DropDown is opened");
        } else {
            test.info("DropDown is closed");
        }
    }

    public void verify_when_click_outside_the_dropdown_for_country() {
        Country.click();
        if (CountryDropDownOutside.isDisplayed()) {
            DropDownOutside.click();
        }
        if (CountryDropDownOutside.isDisplayed()) {
            test.info("Dropdown is still opened");
        } else {
            test.info("Dropdown is opened");
        }
    }

    //---verify country dropdown scrollable ---//
    public void verify_country_dropdown_scrollable() {
        for (int i = 0; i <= CountryValue.getAttribute("value").length(); i++) {
            Country.sendKeys(Keys.ARROW_DOWN);
        }
    }

    public void verify_it_show_selected_option_for_country(){
        Country.click();
        List<WebElement> Countrylist = new ArrayList<>();
        String FirstEntryOfCountry = Countrylist.get(1).getAttribute("value");
        Countrylist.get(1).click();

        String CountryText = Country.getAttribute("value");
        Assert.assertEquals(CountryText,FirstEntryOfCountry);

    }


    public void verify_when_hover_the_close_icon_it_should_show_hand_icon_for_country() {
        String handicon = CountryArrowButton.getCssValue("cursor");
        Assert.assertEquals(handicon, "pointer");
    }

    public void verify_country_in_ascending_order() {
        String country[] = new String[]{CountryValue.getAttribute("value")};
        Arrays.sort(country);
    }

    public void verify_blank_space_removed_from_country() {
        Country.sendKeys(" INDIA ");
        String CountryText = Country.getAttribute("value");
        Assert.assertEquals(CountryText, "INDIA");
        Country.sendKeys(Keys.TAB);
    }

    //---verify state filed text---//
    public void verify_state_filed_name() {
        String StateFiledText = StateFiled.getAttribute("value");
        if (StateFiledText.equals("State")) {
            test.info("Filed name matched");
        } else {
            test.info("Filed name does not matched");
        }

    }


    //---verify state placeholder---//
    public void verify_state_placeholder() {
        String StateText = State.getAttribute("value");
        if (StateText.equals("State")) {
            test.info("Place holder name matched");
        } else {
            test.info("Place holder name does not matched");
        }
    }

    public void verify_state_accept_the_emoji() throws InterruptedException {
        State.click();
        State.sendKeys("\uD83D\uDE12");
        Thread.sleep(1000);
        String StateText = State.getAttribute("value");
        Assert.assertSame(StateText, "");
        State.sendKeys(Keys.TAB);
    }

    public void verify_dropdown_arrow_button_work_for_state() {
        StateArrowButton.isEnabled();
        StateArrowButton.click();
        if (StateValue.isDisplayed()) {
            test.info("DropDown is opened");
        } else {
            test.info("DropDown is closed");
        }
    }

    public void verify_when_click_outside_the_dropdown_for_state() {
        State.click();
        if (StateDropDownOutside.isDisplayed()) {
            StateDropDownOutside.click();
        }
        if (StateDropDownOutside.isDisplayed()) {
            test.info("Dropdown is still opened");
        } else {
            test.info("Dropdown is opened");
        }
    }

    //---verify state dropdown scrollable ---//
    public void verify_state_dropdown_scrollable() {
        for (int i = 0; i <= StateValue.getAttribute("value").length(); i++) {
            State.sendKeys(Keys.ARROW_DOWN);
        }
    }

    public void verify_when_hover_the_close_icon_it_should_show_hand_icon_for_state() {
        String handicon = StateArrowButton.getCssValue("cursor");
        Assert.assertEquals(handicon, "pointer");
    }

    public void verify_state_in_ascending_order() {
        String state[] = new String[]{StateValue.getAttribute("value")};
        Arrays.sort(state);
    }

    public void verify_blank_space_removed_from_state() {
        State.sendKeys(" GUJARAT ");
        String StateText = State.getAttribute("value");
        Assert.assertEquals(StateText, "GUJARAT");
        State.sendKeys(Keys.TAB);
    }
    public void verify_first_entry_of_state(){
        State.click();
        List<WebElement> Statelist = new ArrayList<>();
        String FirstEntryOfState = Statelist.get(0).getAttribute("value");
        Assert.assertEquals(FirstEntryOfState,"Select State");
    }


    public void verify_state_list_should_be_in_ascending(){
        State.click();
        Select se = new Select(State);
        List<String> originalList = new ArrayList();
        originalList.remove(0);
        for (WebElement e : se.getOptions()) {
            originalList.add(e.getAttribute("value"));
        }

        List<String> tempList= originalList;
        Collections.sort(tempList);
        Assert.assertEquals(tempList, originalList);
    }

    public void verify_search_functionality_should_be_there_for_state(){
        State.click();
        if(StateSearch.isDisplayed()==true){
            test.info("Search Functionality available for country code");
        }
        else{
            test.info("Search Functionality does not available for country code ");
        }
    }

    public void verify_it_show_selected_option_for_State(){
        State.click();
        List<WebElement> Statelist = new ArrayList<>();
        String FirstEntryOfState = Statelist.get(1).getAttribute("value");
        Statelist.get(1).click();

        String StateText = State.getAttribute("value");
        Assert.assertEquals(StateText,FirstEntryOfState);

    }

    public void verify_city_filed_name() {
        String CityFiledText = CityFiled.getAttribute("value");
        if (CityFiledText.equals("City")) {
            test.info("Filed name matched");
        } else {
            test.info("Filed name does not matched");
        }

    }

    //---verify city placeholder---//
    public void verify_city_placeholder() {
        String CityText = City.getAttribute("value");
        if (CityText.equals("City")) {
            test.info("Place holder name matched");
        } else {
            test.info("Place holder name does not matched");
        }
    }


    public void verify_first_entry_of_city(){
        City.click();
        List<WebElement> Citylist = new ArrayList<>();
        String FirstEntryOfCity = Citylist.get(0).getAttribute("value");
        Assert.assertEquals(FirstEntryOfCity,"Select City");
    }


    public void verify_city_list_should_be_in_ascending(){
        City.click();
        Select se = new Select(City);
        List<String> originalList = new ArrayList();
        originalList.remove(0);
        for (WebElement e : se.getOptions()) {
            originalList.add(e.getAttribute("value"));
        }

        List<String> tempList= originalList;
        Collections.sort(tempList);
        Assert.assertEquals(tempList, originalList);
    }

    public void verify_search_functionality_should_be_there_for_city(){
        City.click();
        if(CitySearch.isDisplayed()==true){
            test.info("Search Functionality available for city");
        }
        else{
            test.info("Search Functionality does not available for city ");
        }
    }

    public void verify_it_show_selected_option_for_city(){
        City.click();
        List<WebElement> Citylist = new ArrayList<>();
        String FirstEntryOfCity = Citylist.get(1).getAttribute("value");
        Citylist.get(1).click();

        String CityText = City.getAttribute("value");
        Assert.assertEquals(CityText,FirstEntryOfCity);

    }
    public void verify_city_accept_the_emoji() throws InterruptedException {
        City.click();
        City.sendKeys("\uD83D\uDE12");
        Thread.sleep(1000);
        String CityText = City.getAttribute("value");
        Assert.assertSame(CityText, "");
        City.sendKeys(Keys.TAB);
    }

    public void verify_dropdown_arrow_button_work_for_city() {
        CityArrowButton.isEnabled();
        CityArrowButton.click();
        if (CityValue.isDisplayed()) {
            test.info("DropDown is opened");
        } else {
            test.info("DropDown is closed");
        }
    }

    public void verify_when_click_outside_the_dropdown_for_city() {
        City.click();
        if (CityDropDownOutside.isDisplayed()) {
            CityDropDownOutside.click();
        }
        if (CityDropDownOutside.isDisplayed()) {
            test.info("Dropdown is still opened");
        } else {
            test.info("Dropdown is opened");
        }
    }

    //---verify country dropdown scrollable ---//
    public void verify_city_dropdown_scrollable() {
        for (int i = 0; i <= CityValue.getAttribute("value").length(); i++) {
            City.sendKeys(Keys.ARROW_DOWN);
        }
    }

    public void verify_when_hover_the_close_icon_it_should_show_hand_icon_for_city() {
        String handicon = CityArrowButton.getCssValue("cursor");
        Assert.assertEquals(handicon, "pointer");
    }

    public void verify_city_in_ascending_order() {
        String city[] = new String[]{CityValue.getAttribute("value")};
        Arrays.sort(city);
    }

    public void verify_blank_space_removed_from_city() {
        City.sendKeys(" AHMEDABAD ");
        String CityText = City.getAttribute("value");
        Assert.assertEquals(CityText, "AHMEDABAD");
        City.sendKeys(Keys.TAB);
    }


    public void verify_zip_code_filed_name() {
        String ZipCodeText = ZipCodeFiled.getAttribute("value");
        if (ZipCodeText.equals("Zip Code")) {
            test.info("Filed name matched");
        } else {
            test.info("Filed name does not matched");
        }

    }

    //---verify zip code placeholder---//
    public void verify_zip_code_placeholder() {
        String ZipCodeText = ZipCodePlaceHolder.getAttribute("value");
        if (ZipCodeText.equals("Zip Code")) {
            test.info("Place holder name matched");
        } else {
            test.info("Place holder name does not matched");
        }
    }

    public void verify_zip_code_accept_the_emoji() throws InterruptedException {
        ZipCode.click();
        ZipCode.sendKeys("\uD83D\uDE12");
        Thread.sleep(1000);
        String ZipCodeText = ZipCode.getAttribute("value");
        Assert.assertSame(ZipCodeText, "");
        ZipCode.sendKeys(Keys.TAB);
    }

    public void verify_blank_space_removed_from_zip_code() {
        ZipCode.sendKeys(" 380015 ");
        String ZipCodeText = ZipCode.getAttribute("value");
        Assert.assertEquals(ZipCodeText, "380015");
    }

    public void verify_zip_code(String email, String error) throws InterruptedException {
        ZipCode.click();
        ZipCode.sendKeys(email);
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        String ErrorMessageText = ZipCodeError.getAttribute("value");
        if (ErrorMessageText.equals(error)) {
            test.info("Error message matched");
        } else {
            test.info("Error message does not matched");
        }
    }

    public void verify_birthdate_filed_name() {
        String BirthDateText = BirthDateFiled.getText();
        if (BirthDateText.equals("Birthdate") || BirthDateText.equals("DOB") || BirthDateText.equals("Date of Birth")) {
            test.info("Filed name matched");
            
        } else {
            test.info("Filed name does not matched");
        }
    }

    public boolean isCalendarOpen() {

        return Calendar.isDisplayed();
    }

    public void verify_calender_is_opened_or_not() throws InterruptedException {
        CalenderArrowButton.click();
        Thread.sleep(3000);
        if (!isCalendarOpen()) {
            CalenderArrowButton.click();
            isCalendarOpen();
        }

    }

    public void verify_birthdate_formate() throws InterruptedException {
        Utility utils = new Utility();
        CalenderArrowButton.click();
        CalenderArrowButton.click();
        utils.scroll(CalendarValue);
        Thread.sleep(30000);
        CalendarValue.click();
        Thread.sleep(3000);
        boolean isValidFormat = CalenderInputBox.getAttribute("value").matches("([0-9]{2})/([0-9]{2})/([0-9]{4})");
      //  assertTrue(GenericValidator.isDate(CalenderInputBox.getAttribute("value"), "MM/DD/YYYY", false));
        if(isValidFormat == true){
            System.out.println("It's in vallid formate");
        }

    }


    public void verify_date_picker_should_be_current_month_calendar() throws InterruptedException {
        LocalDate CurrentDate = LocalDate.now();
        Month CurrentMonth = CurrentDate.getMonth();

        CalenderArrowButton.click();
        Thread.sleep(5000);
        if (CurrentMonth.equals(CalendarMonth.getAttribute("value"))) {
            test.info("Calendar Opens with Current Month");
        } else {
            test.info("Calendar does not open with current month");
        }

    }

    public void verify_user_should_be_able_to_future_date() {
        LocalDate CurrentDate = LocalDate.now();
        LocalDate futureDate = CurrentDate.plusMonths(3);
        Calendar.sendKeys(String.valueOf(futureDate));
        String CalendarText = Calendar.getAttribute("value");
        Assert.assertNotEquals(CalendarText, String.valueOf(LocalDate.now()));
        Assert.assertEquals(CalendarText, String.valueOf(futureDate));
    }

    public void verify_it_show_selected_option_for_today_date() throws InterruptedException {
        verify_birthdate_formate();
        LocalDate CurrentDate = LocalDate.now();
        String TodayText = CalenderInputBox.getAttribute("value");
        Assert.assertEquals(TodayText,CurrentDate);

    }



    public void verify_previous_and_next_button_work_in_calendar() {
        LocalDate CurrentDate = LocalDate.now();
        Month CurrentMonth = CurrentDate.getMonth();
        CalenderArrowButton.click();
        CalenderArrowButton.click();

        if (CurrentMonth.equals(CalendarMonth.getAttribute("value"))) {
            CalenderPreviousButton.click();
            String PreviousMonthText = CalendarMonth.getAttribute("value");
            Assert.assertEquals(PreviousMonthText, String.valueOf((CurrentMonth.minus(1))));
            CalenderNextButton.click();
            String NextMonthText = CalendarMonth.getAttribute("value");
            System.out.println(NextMonthText);
            Assert.assertEquals(NextMonthText, String.valueOf(CurrentMonth));
        }
    }

    public void verify_when_hover_the_close_icon_it_should_show_hand_icon_for_date_picker() {
        String handicon = CalenderArrowButton.getCssValue("cursor");
        Assert.assertEquals(handicon, "pointer");
    }

    public void verify_phone_number_filed_name() {
        String PhoneNumberText = PhoneNumberFiled.getAttribute("value");
        if (PhoneNumberText.equals("Phone Number") || PhoneNumberText.equals("Mobile Number") || PhoneNumberText.equals("Contact Number")) {
            test.info("Filed name matched");
        } else {
            test.info("Filed name does not matched");
        }
    }

    public void verify_phone_number_place_holder() {
        String PhoneNumberText = PhoneNumberPlaceHolder.getAttribute("value");
        if (PhoneNumberText.equals("Phone Number") || PhoneNumberText.equals("Mobile Number") || PhoneNumberText.equals("Contact Number")) {
            test.info("Placeholder name matched");
        } else {
            test.info("Placeholder name does not matched");
        }
    }

    public void verify_blank_space_removed_from_phone_number() {
        PhoneNumber.sendKeys(" 9327832892 ");
        String PhoneNumberText = PhoneNumber.getAttribute("value");
        Assert.assertEquals(PhoneNumberText, "9327832892");
        PhoneNumber.sendKeys(Keys.TAB);
    }

    public void verify_phonenumber_accept_the_emoji() throws InterruptedException {
        PhoneNumber.click();
        PhoneNumber.sendKeys("\uD83D\uDE12");
        Thread.sleep(1000);
        String PhoneNumberText = PhoneNumber.getAttribute("value");
        Assert.assertSame(PhoneNumberText, "");
        PhoneNumber.sendKeys(Keys.TAB);
    }

    public void verify_phone_number_keyboard_is_numeric() {
        PhoneNumber.click();
        String PhoneNumberKeyBoardType = PhoneNumberKeyBoard.getCssValue("type");
        if (PhoneNumberKeyBoardType.equalsIgnoreCase("number")) {
            test.info("Keyboard is numeric");
        } else {
            test.info("keyboard does not numeric");
        }
    }

    public void verify_phone_number(String phonenumber, String Error) throws InterruptedException {
        PhoneNumber.click();
        PhoneNumber.sendKeys(phonenumber);
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        String ErrorMessageText = PhoneNumberError.getAttribute("value");
        if (ErrorMessageText.equals(Error)) {
            test.info("Error message matched");
        } else {
            test.info("Error message does not matched");
        }
    }

    public void verify_dropdown_arrow_button_work_for_phone_number(){
        PhoneNumberArrowButton.isEnabled();
        PhoneNumberArrowButton.click();
        if (PhoneNumberValue.isDisplayed()) {
            test.info("DropDown is opened");
        } else {
            test.info("DropDown is closed");
        }
    }

    public void verify_when_click_outside_the_dropdown_for_phone_number() {
        PhoneNumber.click();
        if (PhoneNumberDropDownOutside.isDisplayed()) {
            PhoneNumberOutside.click();
        }
        if (PhoneNumberDropDownOutside.isDisplayed()) {
            test.info("Dropdown is still opened");
        } else {
            test.info("Dropdown is opened");
        }
    }


    public void verify_phone_number_dropdown_scrollable() {
        for (int i = 0; i <= PhoneNumberValue.getAttribute("value").length(); i++) {
            PhoneNumber.sendKeys(Keys.ARROW_DOWN);
        }
    }

    public void verify_when_hover_the_close_icon_it_should_show_hand_icon_for_phone_number() {
        String handicon = PhoneNumberArrowButton.getCssValue("cursor");
        Assert.assertEquals(handicon, "pointer");
    }

    public void verify_it_show_selected_option_for_phone_number(){
        PhoneNumber.click();
        List<WebElement> PhoneNumberlist = new ArrayList<>();
        String FirstEntryOfPhoneNumber = PhoneNumberlist.get(1).getAttribute("value");
        PhoneNumberlist.get(1).click();

        String PhoneNumberText = PhoneNumber.getAttribute("value");
        Assert.assertEquals(PhoneNumberText,FirstEntryOfPhoneNumber);

    }
}
