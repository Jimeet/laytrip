package Test;

import Init.BaseClass;
import ObjectRepository.RegisterPage;
import ObjectRepository.SignInPage;
import ObjectRepository.SignUpPage;
import Utils.ReadFromExcel;
import Utils.Utility;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

import static Utils.extentreports.ExtentTestManager.getTest;
import static Utils.extentreports.ExtentTestManager.startTest;


public class SignUpPageTest extends BaseClass {
    Method method = null;

    public SignUpPageTest(){
        super();
    }


    @DataProvider(name = "firstname")
    public Object[][] firstname(){
        ReadFromExcel reader = new ReadFromExcel();
        Utility util = new Utility();
        Object[][] test = reader.read_from_excel(util.get_project_dir()+ "//src//test//java//Data//Data_For_First_Name.xlsx","Sheet1");
        return test;
    }

    @Test(dataProvider = "firstname")
    public void verify_first_name(String Type_of_Data,String FirstName,String Error) throws InterruptedException {
        test = startTest("verify_first_name", "verify_first_name");
        SignUpPage sign_up = new SignUpPage(driver);
        sign_up.click_on_sign_up_button();
        sign_up.verify_first_name_filed_name();
        sign_up.verify_first_name(FirstName,Error);
    }

    @Test
    public void verify_first_name_placeholder() throws InterruptedException {
        test = startTest("verify_first_name_placeholder", "verify_first_name_placeholder");
        SignUpPage sign_up = new SignUpPage(driver);
        sign_up.click_on_sign_up_button();
        sign_up.verify_name_placeholder();
    }
    @Test
    public void verify_first_name_accept_the_emoji() throws InterruptedException {
        test =startTest("verify_first_name_accept_the_emoji", "verify_first_name_accept_the_emoji");
        SignUpPage sign_up = new SignUpPage(driver);
        sign_up.click_on_sign_up_button();
        sign_up.verify_first_name_accept_the_emoji();
    }

    @Test
    public void verify_first_name_with_blank_space() throws InterruptedException {
        test = startTest("verify_first_name_with_blank_space", "verify_first_name_with_blank_space");
        SignUpPage sign_up = new SignUpPage(driver);
        sign_up.click_on_sign_up_button();
        sign_up.verify_first_name_with_blank_space();
    }

    @DataProvider(name = "lastname")
    public Object[][] lastname(){
        ReadFromExcel reader = new ReadFromExcel();
        Utility util = new Utility();
        Object[][] test = reader.read_from_excel(util.get_project_dir()+ "//src//test//java//Data//Data_For_Last_Name.xlsx","Sheet1");
        return test;
    }

    @Test(dataProvider = "lastname")
    public void verify_last_name(String Type_of_Data,String LastName,String Error) throws InterruptedException {
        test =startTest("verify_last_name", "verify_last_name");
        SignUpPage sign_up = new SignUpPage(driver);
        sign_up.click_on_sign_up_button();
        sign_up.verify_last_name_filed_name();
        sign_up.verify_last_name(LastName,Error);
    }

    @Test
    public void verify_last_name_placeholder() throws InterruptedException {
        test = startTest("verify_last_name_placeholder", "verify_last_name_placeholder");
        SignUpPage sign_up = new SignUpPage(driver);
        sign_up.click_on_sign_up_button();
        sign_up.verify_last_name_placeholder();
    }
    @Test
    public void verify_last_name_accept_the_emoji() throws InterruptedException {
        test = startTest("verify_last_name_accept_the_emoji", "verify_last_name_accept_the_emoji");
        SignUpPage sign_up = new SignUpPage(driver);
        sign_up.click_on_sign_up_button();
        sign_up.verify_last_name_accept_the_emoji();
    }

    @Test
    public void verify_last_name_with_blank_space() throws InterruptedException {
        test = startTest("verify_last_name_with_blank_space", "verify_last_name_with_blank_space");
        SignUpPage sign_up = new SignUpPage(driver);
        sign_up.click_on_sign_up_button();
        sign_up.verify_last_name_with_blank_space();
    }

    @DataProvider(name = "email")
    public Object[][] email(){
        ReadFromExcel reader = new ReadFromExcel();
        Utility util = new Utility();
        Object[][] test = reader.read_from_excel(util.get_project_dir()+ "//src//test//java//Data//Data_For_Email.xlsx","Sheet1");
        return test;
    }

    @Test(dataProvider = "email")
    public void verify_email_filed(String Type_of_Data,String Email,String Error) throws InterruptedException {
        test =startTest("verify_email_filed", "verify_email_filed");
        SignUpPage sign_up = new SignUpPage(driver);
        SignInPage sign_in = new SignInPage(driver);
        sign_up.click_on_sign_up_button();
        sign_in.verify_email_filed_name();
        sign_in.verify_email(Email,Error);
    }

    @Test
    public void verify_email_placeholder() throws InterruptedException {
        test = startTest("verify_email_placeholder", "verify_email_placeholder");
        SignUpPage sign_up = new SignUpPage(driver);
        SignInPage sign_in = new SignInPage(driver);
        sign_up.click_on_sign_up_button();
        sign_in.verify_email_placeholder();
    }

    @Test
    public void verify_email_accept_the_emoji() throws InterruptedException {
        test =startTest("verify_email_placeholder", "verify_email_placeholder");

        SignUpPage sign_up = new SignUpPage(driver);
        SignInPage sign_in = new SignInPage(driver);
        sign_up.click_on_sign_up_button();
        sign_in.verify_email_accept_the_emoji();
    }

    @Test
    public void verify_email_with_blank_space() throws InterruptedException {
        test =startTest("verify_email_with_blank_space", "verify_email_with_blank_space");
        SignUpPage sign_up = new SignUpPage(driver);
        SignInPage sign_in = new SignInPage(driver);
        sign_up.click_on_sign_up_button();
        sign_in.verify_email_with_blank_space();
    }

    @Test
    public void verify_password_with_blank_space() throws InterruptedException {
        test = startTest("verify_password_with_blank_space", "verify_password_with_blank_space");
        SignUpPage sign_up = new SignUpPage(driver);
        sign_up.click_on_sign_up_button();
        sign_up.verify_password_filed_name();
        sign_up.verify_password_with_blank_space();
    }


    @Test
    public void verify_password_placeholder() throws InterruptedException {
        test = startTest("verify_password_placeholder", "verify_password_placeholder");
        SignUpPage sign_up = new SignUpPage(driver);
        sign_up.click_on_sign_up_button();
        sign_up.verify_password_placeholder();
    }

    @Test
    public void verify_password_eye_button() throws InterruptedException {
        test = startTest("verify_password_eye_button", "verify_password_eye_button");
        SignUpPage sign_up = new SignUpPage(driver);
        sign_up.click_on_sign_up_button();
        sign_up.verify_password_eye_button();
        sign_up.verify_password_eye_hide_button();
      //  sign_up.verify_password_filed_should_be_secure();
    //    sign_up.verify_password_accept_the_emoji();
        sign_up.verify_hand_icon_hover_over_the_eye_icon();
    }

    @Test
    public void verify_date_of_birth() throws InterruptedException {
        test =startTest("verify_password_eye_button", "verify_password_eye_button");
        RegisterPage birth_date = new RegisterPage(driver);
        SignUpPage sign_up = new SignUpPage(driver);
        sign_up.click_on_sign_up_button();
        birth_date.verify_birthdate_filed_name();
        birth_date.verify_calender_is_opened_or_not();
        birth_date.verify_birthdate_formate();
        birth_date.verify_date_picker_should_be_current_month_calendar();
       // birth_date.verify_user_should_be_able_to_future_date();
        birth_date.verify_previous_and_next_button_work_in_calendar();
        birth_date.verify_it_show_selected_option_for_today_date();
        birth_date.verify_when_hover_the_close_icon_it_should_show_hand_icon_for_date_picker();

    }


    @Test
    public void verify_signup_with_valid_data() throws InterruptedException {
        test = startTest("verify_signup_with_valid_data", "verify_signup_with_valid_data");
        SignUpPage sign_up = new SignUpPage(driver);
        sign_up.verify_signup_with_valid_data();


    }

}
