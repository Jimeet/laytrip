package Test;

import Init.BaseClass;
import ObjectRepository.RegisterPage;
import ObjectRepository.SignInPage;
import ObjectRepository.SignUpPage;
import Utils.ReadFromExcel;

import Utils.Utility;
import Utils.extentreports.ExtentTestManager;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import io.netty.util.internal.logging.InternalLogger;

import org.openqa.selenium.Cookie;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.tinylog.Logger;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.logging.LogRecord;

import static Utils.extentreports.ExtentManager.extentReports;
import static Utils.extentreports.ExtentTestManager.getTest;
import static Utils.extentreports.ExtentTestManager.startTest;
import static org.apache.logging.log4j.LogManager.getLogger;


public class SignInPageTest extends BaseClass {





    public SignInPageTest(){
        super();
    }




    @DataProvider(name = "email")
    public Object[][] email(){
        ReadFromExcel reader = new ReadFromExcel();
        Utility util = new Utility();
        Object[][] test = reader.read_from_excel(util.get_project_dir()+ "//src//test//java//Data//Data_For_Email.xlsx","Sheet1");
        return test;
    }

    @Test(dataProvider = "email")
    public void verify_email_filed(String Type_of_Data,String Email,String Error) throws InterruptedException {
        test = extent.createTest("verify for test email","emakdjs");
        test = startTest("verify_email_filed", "verify_email_filed");
        SignInPage sign_in = new SignInPage(driver);
        sign_in.click_on_sign_in_button();
        sign_in.verify_email_filed_name();
        sign_in.verify_email(Email,Error);
        //extentReports.addTestRunnerOutput((List<String>) new Throwable());

    }

    @Test
    public void verify_email_placeholder() throws InterruptedException {
        test = startTest("verify_email_placeholder", "verify_email_placeholder");
        SignInPage sign_in = new SignInPage(driver);
        sign_in.click_on_sign_in_button();
        sign_in.verify_email_placeholder();
    }

    @Test
    public void verify_email_accept_the_emoji() throws InterruptedException {
        test = startTest("verify_email_accept_the_emoji", "verify_email_accept_the_emoji");
        SignInPage sign_in = new SignInPage(driver);
        sign_in.click_on_sign_in_button();
        sign_in.verify_email_accept_the_emoji();
    }

    @Test
    public void verify_email_with_blank_space() throws InterruptedException {
        test = startTest("verify_email_with_blank_space", "verify_email_with_blank_space");
        SignInPage sign_in = new SignInPage(driver);
        sign_in.click_on_sign_in_button();
        sign_in.verify_email_with_blank_space();
    }

    @Test
    public void verify_password_with_blank_space() throws InterruptedException {
        //test.createNode("asdad");
        //test.generateLog(Status.FAIL,"ww");
        test =startTest("Verify_SignIn_Page", "verify_password_with_blank_space");
        SignUpPage sign_up = new SignUpPage(driver);
        SignInPage sign_in = new SignInPage(driver);
        sign_in.click_on_sign_in_button();
        sign_up.verify_password_filed_name();
        sign_up.verify_password_with_blank_space();
    }


    @Test
    public void verify_password_placeholder() throws InterruptedException {
        test =startTest("verify_password_placeholder", "verify_password_placeholder");
        SignUpPage sign_up = new SignUpPage(driver);
        SignInPage sign_in = new SignInPage(driver);
        sign_in.click_on_sign_in_button();
        sign_up.verify_password_placeholder();
    }

    @Test
    public void verify_password_eye_button() throws InterruptedException {
        test =startTest("verify_password_eye_button", "verify_password_eye_button");
        SignUpPage sign_up = new SignUpPage(driver);
        SignInPage sign_in = new SignInPage(driver);
        sign_in.click_on_sign_in_button();
        sign_up.verify_password_eye_button();
        sign_up.verify_password_eye_hide_button();
        //  sign_up.verify_password_filed_should_be_secure();
        //    sign_up.verify_password_accept_the_emoji();
        sign_up.verify_hand_icon_hover_over_the_eye_icon();
    }


    @Test
    public void sign_in_with_valid_data() throws InterruptedException {
        test = startTest("sign_in_with_valid_data", "sign_in_with_valid_data");
        SignInPage sign_in = new SignInPage(driver);
        sign_in.sign_in_with_valid_data();
    }

}
