package Utils;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openxmlformats.schemas.drawingml.x2006.main.ThemeDocument;
import org.testng.annotations.BeforeTest;

import static Init.BaseClass.driver;

public class Utility {
    public String get_project_dir(){
        String dir = System.getProperty("user.dir");
        return dir;
    }

    public void scroll(WebElement element) throws InterruptedException {
        Thread.sleep(3000);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView(true);", element);
    }


}
