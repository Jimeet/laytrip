package Init;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;


import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;



public class BaseClass {
    public static ExtentSparkReporter htmlReporter;
    public static ExtentReports extent;
    public static ExtentTest test;

    public static WebDriver driver;
    Properties prop;


    //--- For the open the chrome before every test case and set the url ---//
    @BeforeMethod
    public void init(){


        FileInputStream fileInput = null;

            File file = new File("src//test//java//Config//config.properties");

            try {
                fileInput = new FileInputStream(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
            prop = new Properties();

            //load properties file
            try {
                prop.load(fileInput);
                if(prop.getProperty("BrowserName").equalsIgnoreCase("chrome")){
                    WebDriverManager.chromedriver().setup();
                    driver = new ChromeDriver();
                    driver.manage().window().maximize();
                    driver.get(prop.getProperty("url"));
                }
                if(prop.getProperty("BrowserName").equalsIgnoreCase("Firefox")){
                    WebDriverManager.chromiumdriver().setup();
                    driver = new FirefoxDriver();
                    driver.manage().window().maximize();
                    driver.get(prop.getProperty("url"));

                }
            } catch (IOException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
    }

    @BeforeTest
    public void startReport(){
        htmlReporter =  new ExtentSparkReporter(System.getProperty("user.dir")+"/extent-reports/extent-report.html");

        extent = new ExtentReports();
        extent.attachReporter(htmlReporter);

        htmlReporter.config().setDocumentTitle("report");
        htmlReporter.config().setReportName("automation report");

    }

    @AfterMethod
    public void getResult(ITestResult result){
        if(result.getStatus() == ITestResult.FAILURE){
            test.log(Status.FAIL,result.getThrowable());
        } else if (result.getStatus()==ITestResult.SUCCESS) {test.log(Status.PASS, result.getTestName());}
        else {test.log(Status.SKIP, result.getTestName());}
        }


    //--- For the Close the chrome ---//
    @AfterMethod
    public void Teardown(){
        driver.quit();
        extent.flush();
    }



    public static String screenShot(WebDriver driver,String filename) {
        String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
        TakesScreenshot takesScreenshot = (TakesScreenshot) driver;
        File source = takesScreenshot.getScreenshotAs(OutputType.FILE);
        String destination = System.getProperty("user.dir")+"\\ScreenShot\\"+filename+"_"+dateName+".png";
        File finalDestination= new File(destination);
        try {
            FileUtils.copyFile(source, finalDestination);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.getMessage();
        }
        return destination;
    }

    public static String getCurrentTime() {
        String currentDate = new SimpleDateFormat("yyyy-MM-dd-hhmmss").format(new Date());
        return currentDate;
    }
}

