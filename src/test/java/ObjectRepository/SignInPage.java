package ObjectRepository;

import Init.BaseClass;
import com.aventstack.extentreports.model.ExceptionInfo;
import com.aventstack.extentreports.model.Log;
import com.sun.jdi.StackFrame;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.bidi.log.StackTrace;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.Reporter;
import org.tinylog.Logger;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import java.lang.reflect.Method;

public class SignInPage extends BaseClass {


    public SignInPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(xpath = "//span[text()='Sign In']")
    WebElement SignInButton;

    @FindBy(id = "dropdown-basic")
    WebElement SignInDropdown;

    @FindBy(xpath = "//input[@name = 'email']")
    WebElement InputEmail;

    @FindBy(xpath = "/html/body/div[5]/div/div/div[2]/form/div[1]/div[3]/p")
    WebElement EmailErrorMessage;

    @FindBy(xpath = "/html/body/div[4]/div/div/div[2]/form/div[1]/p")
    WebElement EmailErrorMessageSignIn;

    @FindBy(xpath = "//label[text()='Email Address']")
    WebElement EmailAddressFiled;

    @FindBy(xpath = "//label[text()='Email Address']")
    WebElement EmailAddressPlaceHolder;

    @FindBy(id = "dropdown-basic")
    WebElement DasboardDropDown;

    @FindBy(xpath = "//a[text()='Collections']")
    WebElement CollectionText;


    public void click_on_sign_in_button() throws InterruptedException {
        Thread.sleep(10000);
        SignInDropdown.click();
        Thread.sleep(10000);
        SignInButton.click();
        Thread.sleep(10000);

    }

    public void verify_email(String email,String error) throws InterruptedException {
        SignUpPage signup = new SignUpPage(driver);
        StackTraceElement[] stackTrace = Thread.currentThread()
                .getStackTrace();
        InputEmail.click();
        InputEmail.sendKeys(email);
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        signup.SignUpPageButton.click();
        String EmailErrorMessageText;
        if(stackTrace[2].getFileName() =="SignInPageTest.java"){
            EmailErrorMessageText = EmailErrorMessageSignIn.getAttribute("value");
        }
        else {
            EmailErrorMessageText = EmailErrorMessage.getAttribute("value");
        }
        Assert.assertEquals(EmailErrorMessageText,error);
        Thread.sleep(3000);



    }


    //---verify email filed text---//
    public void verify_email_filed_name(){
        InputEmail.click();
        String EmailAddressText = EmailAddressFiled.getText();
        if(EmailAddressText.equals("Email") || EmailAddressText.equals("Email Address")){
            test.info("Filed name matched");
        }else{
            test.info("Filed name does not matched");
        }
    }

    //---verify email placeholder---//
    public void verify_email_placeholder(){
        String EmailAddressPlaceHolderText = EmailAddressPlaceHolder.getText();
        if(EmailAddressPlaceHolderText.equals("Email") || EmailAddressPlaceHolderText.equals("Email Address")){
            test.info("Place holder name matched");
        }else{
            test.info("Place holder name does not matched");
        }
    }


    //--- verify email accept the emoji ---//
    public void verify_email_accept_the_emoji() throws InterruptedException {
        SignUpPage signup = new SignUpPage(driver);
        InputEmail.click();
        InputEmail.sendKeys("\uD83D\uDE12");
        Thread.sleep(1000);
        signup.SignUpPageButton.click();
        String EmailText = InputEmail.getAttribute("value");
        Assert.assertSame(EmailText,"");
    }


    //--- verify email with blank space  ---//
    public void verify_email_with_blank_space() throws InterruptedException {
        InputEmail.click();
        InputEmail.sendKeys(" test@gmail.com ");
        String EmailText = InputEmail.getAttribute("value");
        Assert.assertNotEquals(EmailText," test@gmail.com ");
        InputEmail.sendKeys(Keys.TAB);
        Thread.sleep(3000);
    }

    public void sign_in_with_valid_data() throws InterruptedException {
        SignUpPage sign_up = new SignUpPage(driver);
        click_on_sign_in_button();
        InputEmail.click();
        InputEmail.sendKeys("Test5@gmail.com");
        sign_up.Password.click();
        sign_up.Password.sendKeys("Test@123");
        sign_up.SignUpPageButton.click();
        Thread.sleep(5000);
        DasboardDropDown.click();
        CollectionText.isDisplayed();


    }


}
