package ObjectRepository;

import Init.BaseClass;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.Reporter;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;


public class SignUpPage extends BaseClass {

    public SignUpPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }


    @FindBy(xpath = "//span[text()='Sign In']")
    WebElement SignInButton;

    @FindBy(id = "dropdown-basic")
    WebElement SignInDropdown;

    @FindBy(xpath = "//b[text()='Sign Up']")
    WebElement SignUPButton;

    @FindBy(xpath = "//button[@type= 'submit']")
    WebElement SignUpPageButton;

    @FindBy(xpath = "//input[@name = 'firstName']")
    WebElement FirstName;

    @FindBy(xpath = "//label[text() = 'First Name']")
    WebElement FirstNameFiled;

    @FindBy(xpath = "/html/body/div[5]/div/div/div[2]/form/div[1]/div[1]/p")
    WebElement FirstNameErrorMessage;

    @FindBy(xpath = "/html/body/div[5]/div/div/div[2]/form/div[1]/div[2]/p")
    WebElement LastNameErrorMessage;

    @FindBy(xpath = "//label[text() = 'First Name']")
    WebElement FirstNamePlaceholder;


    @FindBy(xpath = "xpath of middle name")
    WebElement MiddleName;

    @FindBy(xpath = "xpath of middle name filed")
    WebElement MiddleNameFiled;

    @FindBy(xpath = "xpath of middle name placeholder")
    WebElement MiddleNamePlaceholder;

    @FindBy(xpath = "//input[@name = 'lastName']")
    WebElement LastName;

    @FindBy(xpath = "//label[text() = 'Last Name']")
    WebElement LastNameFiled;

    @FindBy(xpath = "//label[text() = 'Last Name']")
    WebElement LastNamePlaceholder;

    @FindBy(xpath = "xpath of user name")
    WebElement UserName;

    @FindBy(xpath = "xpath of user name filed")
    WebElement UserNameFiled;

    @FindBy(xpath = "xpath of User name placeholder")
    WebElement UserNamePlaceholder;

    @FindBy(xpath = "xpath of nick name")
    WebElement NickName;

    @FindBy(xpath = "xpath of nick name filed")
    WebElement NickNameFiled;

    @FindBy(xpath = "xpath of nick name placeholder")
    WebElement NickNamePlaceholder;

    @FindBy(xpath = "xpath of full name")
    WebElement FullName;

    @FindBy(xpath = "xpath of full name filed")
    WebElement FullNameFiled;

    @FindBy(xpath = "xpath of full name placeholder")
    WebElement FullNamePlaceholder;

    @FindBy(xpath = "//input[@name = 'password']")
    WebElement Password;

    @FindBy(xpath = "//label[text() = 'Password']")
    WebElement PasswordFiled;

    @FindBy(xpath = "//label[text() = 'Password']")
    WebElement PasswordPlaceholder;

    @FindBy(xpath = "//button[@class ='btn btn-outline-primary password_eye']")
    WebElement PasswordEyeButton;

    @FindBy(xpath = "//img[@src='/hide_eye.svg']")
    WebElement PasswordEyeHideButton;

    @FindBy(xpath = "//img[@src='../hide_eye.svg']")
    WebElement PasswordEyeHideButtonSignup;

    @FindBy(xpath = "xpath of confirm password")
    WebElement ConfirmPassword;

    @FindBy(xpath = "xpath of confirm password filed")
    WebElement ConfirmPasswordFiled;

    @FindBy(xpath = "xpath of confirm password filed placeholder")
    WebElement ConfirmPasswordPlaceholder;

    @FindBy(xpath = "xpath of confirm password eye button")
    WebElement ConfirmPasswordEyeButton;

    @FindBy(xpath = "xpath of confirm password eye hide button")
    WebElement ConfirmPasswordEyeHideButton;

    @FindBy(xpath = "xpath of new password")
    WebElement NewPassword;

    @FindBy(xpath = "xpath of new password filed")
    WebElement NewPasswordFiled;

    @FindBy(xpath = "xpath of new password filed placeholder")
    WebElement NewPasswordPlaceholder;

    @FindBy(xpath = "xpath of new password eye button")
    WebElement NewPasswordEyeButton;

    @FindBy(xpath = "xpath of new password eye hide button")
    WebElement NewPasswordEyeHideButton;

    @FindBy(xpath = "xpath of current password")
    WebElement CurrentPassword;

    @FindBy(xpath = "xpath of current password filed")
    WebElement CurrentPasswordFiled;

    @FindBy(xpath = "xpath of current password filed placeholder")
    WebElement CurrentPasswordPlaceholder;

    @FindBy(xpath = "xpath of current password eye button")
    WebElement CurrentPasswordEyeButton;

    @FindBy(xpath = "xpath of current password eye hide button")
    WebElement CurrentPasswordEyeHideButton;

    @FindBy(xpath = "xpath of old password")
    WebElement OldPassword;

    @FindBy(xpath = "xpath of old password filed")
    WebElement OldPasswordFiled;

    @FindBy(xpath = "xpath of old password filed placeholder")
    WebElement OldPasswordPlaceholder;

    @FindBy(xpath = "xpath of old password eye button")
    WebElement OldPasswordEyeButton;

    @FindBy(xpath = "xpath of old password eye hide button")
    WebElement OldPasswordEyeHideButton;

    @FindBy(xpath = "//button[text()='Continue ']")
    WebElement VerifyAccount;


    public void click_on_sign_up_button() throws InterruptedException {
        Thread.sleep(5000);
        SignInDropdown.click();
        Thread.sleep(5000);
        SignInButton.click();
        Thread.sleep(5000);
        SignUPButton.click();
        Thread.sleep(5000);

    }

    public void verify_first_name(String firstname,String error) throws InterruptedException {
        FirstName.click();
        FirstName.sendKeys(firstname);
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        SignUpPageButton.click();
        String ErrorMessageText = FirstNameErrorMessage.getText();
        Assert.assertEquals(error,ErrorMessageText);
        Thread.sleep(3000);

    }

    //---verify first name filed text---//
    public void verify_first_name_filed_name(){
        String FirstNameText = FirstNameFiled.getText();
        if(FirstNameText.equals("First Name")){
            test.info("Filed name matched");
        }else{
            test.info("Filed name does not matched");
        }
    }

    //---verify first name placeholder---//
    public void verify_name_placeholder(){
        String FirstNamePlaceHolderText = FirstNamePlaceholder.getText();
        if(FirstNamePlaceHolderText.equals("First Name")){
            test.info("Place holder name matched");
        }else{
            test.info("Place holder name does not matched");
        }
    }

    public void click_on_sign_up_button_for_sign_up_page(){
        SignUpPageButton.click();
    }


    //--- verify first name accept the emoji ---//
    public void verify_first_name_accept_the_emoji() throws InterruptedException {
        FirstName.click();
        FirstName.sendKeys("\uD83D\uDE12");
        click_on_sign_up_button_for_sign_up_page();
        String FirstNameErrorText = FirstNameErrorMessage.getAttribute("value");
        Assert.assertSame(FirstNameErrorText,"Emoji should not allowed in email.");

    }


    //--- verify first name with blank space  ---//
    public void verify_first_name_with_blank_space()throws InterruptedException {
        FirstName.click();
        FirstName.sendKeys(" TEST ");
        String FirstNameText = FirstName.getAttribute("value");
        Assert.assertNotEquals(FirstNameText," TEST ");
        FirstName.sendKeys(Keys.TAB);
        Thread.sleep(3000);
    }

    public void verify_middle_name(String middlename)  {
        MiddleName.click();
        MiddleName.sendKeys(middlename);
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        test.info("Add the email more than 350 characters");
//        FirstName.sendKeys(Keys.TAB);
//        String ErrorMessageText = ErrorMessage.getAttribute("value");
//        if(ErrorMessageText.equals("Email must be maximum 320 characters long.") || ErrorMessageText.equals("Email address must be maximum 320 characters long.")){
//            test.info("Error message matched");
//        }else{
//            test.info("Error message does not matched");
//        }

    }

    //---verify middle name filed text---//
    public void verify_middle_name_filed_name(){
        String MiddleNameText = MiddleNameFiled.getAttribute("value");
        if(MiddleNameText.equals("First Name")){
            test.info("Filed name matched");
        }else{
            test.info("Filed name does not matched");
        }
    }

    //---verify middle name placeholder---//
    public void verify_middle_name_placeholder(){
        String MiddleNamePlaceHolderText = MiddleNamePlaceholder.getAttribute("value");
        if(MiddleNamePlaceHolderText.equals("Middle Name")){
            test.info("Place holder name matched");
        }else{
            test.info("Place holder name does not matched");
        }
    }


    //--- verify middle name accept the emoji ---//
    public void verify_middle_name_accept_the_emoji() throws InterruptedException {
        MiddleName.click();
        CharSequence text = "'this my text \uD83E\uDD2F\uD83D\uDE21\uD83D\uDDE3\uD83D\uDC63' ";
        MiddleName.sendKeys(text);
        String MiddleNameText = MiddleName.getAttribute("value");
        System.out.println("testts"+MiddleNameText+"sss");
        Assert.assertSame(MiddleNameText,"Emoji should not allowed in email.");
    }


    //--- verify middle name with blank space  ---//
    public void verify_middle_name_with_blank_space() {
        MiddleName.click();
        MiddleName.sendKeys(" TEST ");
        String MiddleNameText = MiddleName.getAttribute("value");
        Assert.assertNotEquals(MiddleNameText," TEST ");
    }

    public void verify_last_name(String lastname,String error) throws InterruptedException {

        LastName.sendKeys(lastname);
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        SignUpPageButton.click();
        String ErrorMessageText = LastNameErrorMessage.getText();
        Assert.assertEquals(error,ErrorMessageText);
        Thread.sleep(3000);

    }

    //---verify last name filed text---//
    public void verify_last_name_filed_name(){
        LastName.click();
        String LastNameText = LastNameFiled.getText();
        if(LastNameText.equals("Last Name")){
            test.info("Filed name matched");
        }else{
            test.info("Filed name does not matched");
        }
    }

    //---verify last name placeholder---//
    public void verify_last_name_placeholder(){
        String LastNamePlaceHolderText = LastNamePlaceholder.getText();
        if(LastNamePlaceHolderText.equals("Last Name")){
            test.info("Place holder name matched");
        }else{
            test.info("Place holder name does not matched");
        }
    }


    //--- verify last name accept the emoji ---//
    public void verify_last_name_accept_the_emoji() throws InterruptedException {
        LastName.click();
        CharSequence text = "'this my text \uD83E\uDD2F\uD83D\uDE21\uD83D\uDDE3\uD83D\uDC63' ";
        LastName.sendKeys(text);
        String LastNameText = LastName.getAttribute("value");
        System.out.println("testts"+LastNameText+"sss");
        Assert.assertSame(LastNameText,"Emoji should not allowed in email.");
    }


    //--- verify last name with blank space  ---//
    public void verify_last_name_with_blank_space() throws InterruptedException {
        LastName.click();
        LastName.sendKeys(" TEST ");
        String LastNameText = LastName.getAttribute("value");
        Assert.assertNotEquals(LastNameText," TEST ");
        LastName.sendKeys(Keys.TAB);
        Thread.sleep(3000);
    }


    public void verify_user_name(String username) throws InterruptedException {
        UserName.click();
        UserName.sendKeys(username);
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        test.info("Add the email more than 350 characters");
//        FirstName.sendKeys(Keys.TAB);
//        String ErrorMessageText = ErrorMessage.getAttribute("value");
//        if(ErrorMessageText.equals("Email must be maximum 320 characters long.") || ErrorMessageText.equals("Email address must be maximum 320 characters long.")){
//            test.info("Error message matched");
//        }else{
//            test.info("Error message does not matched");
//        }

    }

    //---verify user name filed text---//
    public void verify_user_name_filed_name(){
        String UserNameText = UserNameFiled.getAttribute("value");
        if(UserNameText.equals("Username")){
            test.info("Filed name matched");
        }else{
            test.info("Filed name does not matched");
        }
    }

    //---verify user name placeholder---//
    public void verify_user_name_placeholder(){
        String UserNamePlaceHolderText = UserNamePlaceholder.getAttribute("value");
        if(UserNamePlaceHolderText.equals("Username")){
            test.info("Place holder name matched");
        }else{
            test.info("Place holder name does not matched");
        }
    }


    //--- verify user name accept the emoji ---//
    public void verify_user_name_accept_the_emoji() throws InterruptedException {
        UserName.click();
        CharSequence text = "'this my text \uD83E\uDD2F\uD83D\uDE21\uD83D\uDDE3\uD83D\uDC63' ";
        UserName.sendKeys(text);
        String UserNameText = UserName.getAttribute("value");
        System.out.println("testts"+UserNameText+"sss");
        Assert.assertSame(UserNameText,"Emoji should not allowed in email.");
    }


    //--- verify user name with blank space  ---//
    public void verify_user_name_with_blank_space() {
        UserName.click();
        UserName.sendKeys(" TEST ");
        String UserNameText = UserName.getAttribute("value");
        Assert.assertNotEquals(UserNameText," TEST ");
    }

    public void verify_nick_name(String nickname) throws InterruptedException {
        NickName.click();
        NickName.sendKeys(nickname);
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        test.info("Add the email more than 350 characters");
//        FirstName.sendKeys(Keys.TAB);
//        String ErrorMessageText = ErrorMessage.getAttribute("value");
//        if(ErrorMessageText.equals("Email must be maximum 320 characters long.") || ErrorMessageText.equals("Email address must be maximum 320 characters long.")){
//            test.info("Error message matched");
//        }else{
//            test.info("Error message does not matched");
//        }

    }

    //---verify nick name filed text---//
    public void verify_nick_name_filed_name(){
        String NickNameText = NickNameFiled.getAttribute("value");
        if(NickNameText.equals("Nickname")){
            test.info("Filed name matched");
        }else{
            test.info("Filed name does not matched");
        }
    }

    //---verify nick name placeholder---//
    public void verify_nick_name_placeholder(){
        String NickNamePlaceHolderText = NickNamePlaceholder.getAttribute("value");
        if(NickNamePlaceHolderText.equals("Nickname")){
            test.info("Place holder name matched");
        }else{
            test.info("Place holder name does not matched");
        }
    }


    //--- verify nick name accept the emoji ---//
    public void verify_nick_name_accept_the_emoji() throws InterruptedException {
        NickName.click();
        CharSequence text = "'this my text \uD83E\uDD2F\uD83D\uDE21\uD83D\uDDE3\uD83D\uDC63' ";
        NickName.sendKeys(text);
        String NickNameText = NickName.getAttribute("value");
        System.out.println("testts"+NickNameText+"sss");
        Assert.assertSame(NickNameText,"Emoji should not allowed in email.");
    }


    //--- verify nick name with blank space  ---//
    public void verify_nick_name_with_blank_space() {
        NickName.click();
        NickName.sendKeys(" TEST ");
        String NickNameText = NickName.getAttribute("value");
        Assert.assertNotEquals(NickNameText," TEST ");
    }

    public void verify_full_name(String fullname) throws InterruptedException {
        FullName.click();
        FullName.sendKeys(fullname);
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        test.info("Add the email more than 350 characters");
//        FirstName.sendKeys(Keys.TAB);
//        String ErrorMessageText = ErrorMessage.getAttribute("value");
//        if(ErrorMessageText.equals("Email must be maximum 320 characters long.") || ErrorMessageText.equals("Email address must be maximum 320 characters long.")){
//            test.info("Error message matched");
//        }else{
//            test.info("Error message does not matched");
//        }

    }

    //---verify full name filed text---//
    public void verify_full_name_filed_name(){
        String FullNameText = FullNameFiled.getAttribute("value");
        if(FullNameText.equals("Full Name")){
            test.info("Filed name matched");
        }else{
            test.info("Filed name does not matched");
        }
    }

    //---verify full name placeholder---//
    public void verify_full_name_placeholder(){
        String FullNamePlaceHolderText = FullNamePlaceholder.getAttribute("value");
        if(FullNamePlaceHolderText.equals("Full Name")){
            test.info("Place holder name matched");
        }else{
            test.info("Place holder name does not matched");
        }
    }


    //--- verify full name accept the emoji ---//
    public void verify_full_name_accept_the_emoji() throws InterruptedException {
        FullName.click();
        CharSequence text = "'this my text \uD83E\uDD2F\uD83D\uDE21\uD83D\uDDE3\uD83D\uDC63' ";
        FullName.sendKeys(text);
        String FullNameText = FullName.getAttribute("value");
        System.out.println("testts"+FullNameText+"sss");
        Assert.assertSame(FullNameText,"Emoji should not allowed in email.");
    }


    //--- verify full name with blank space  ---//
    public void verify_full_name_with_blank_space() {
        FullName.click();
        FullName.sendKeys(" TEST ");
        String FullNameText = FullName.getAttribute("value");
        Assert.assertNotEquals(FullNameText," TEST ");
    }

    //---verify password filed text---//
    public void verify_password_filed_name(){
        String PasswordText = PasswordFiled.getText();
        if(PasswordText.equals("Password")){
            test.info("Filed name matched");
        }else{
            test.info("Filed name does not matched");
        }
    }


    //---verify password placeholder---//
    public void verify_password_placeholder(){
        String PasswordPlaceHolderText = PasswordPlaceholder.getText();
        if(PasswordPlaceHolderText.equals("Password")){
            test.info("Place holder name matched");
        }else{
            test.info("Place holder name does not matched");
        }
    }

    //--- verify password with blank space  ---//
    public void verify_password_with_blank_space() throws InterruptedException {
        Password.click();
        Password.sendKeys(" TEST@123 ");
        String PasswordText = Password.getAttribute("value");
        Assert.assertEquals(PasswordText,"TEST@123");
        Password.sendKeys(Keys.TAB);
        Thread.sleep(3000);
    }


    //--- verify password eye button ---//
    public void verify_password_eye_button(){
        Password.click();
        if(PasswordEyeButton.isDisplayed()){
            test.info("Password eye button is displayed");
        }else {
            test.info("Password eye button does not displayed");
        }

    }

    //--- verify password eye button in hide mode ---//
    public void verify_password_eye_hide_button(){
        SignUpPage signup = new SignUpPage(driver);
        StackTraceElement[] stackTrace = Thread.currentThread()
                .getStackTrace();
        if(stackTrace[2].getFileName() =="SignInPageTest.java"){
            if(PasswordEyeHideButton.isDisplayed()){
            test.info("Password eye button is in hide mode");
        }else {
            test.info("Password eye button does not hide");
        }}else {
            PasswordEyeHideButtonSignup.isDisplayed();
        }

    }

    //--- verify password filed should be secure  ---//
    public void verify_password_filed_should_be_secure() {
        Password.click();
        Password.sendKeys("TEST@123");
        Password.sendKeys(Keys.TAB);
        String PasswordText = Password.getAttribute("value");
        Assert.assertEquals(PasswordText,"........");
    }

    //--- verify password accept the emoji ---//
    public void verify_password_accept_the_emoji() throws InterruptedException {
        Password.click();
        Password.sendKeys("\uD83D\uDE12");
        Thread.sleep(1000);
        String PasswordText = Password.getAttribute("value");
        Assert.assertSame(PasswordText,"");
        Password.sendKeys(Keys.TAB);
        Thread.sleep(1000);
    }


    //--- verify hand icon show hover over the eye icon ---//
    public void verify_hand_icon_hover_over_the_eye_icon() throws InterruptedException {
        String handicon = PasswordEyeButton.getCssValue("cursor");
        Assert.assertEquals(handicon, "pointer");
        Thread.sleep(3000);
    }

    //---verify confirm password filed text---//
    public void verify_confirm_password_filed_name(){
        String ConfirmPasswordText = ConfirmPasswordFiled.getAttribute("value");
        if(ConfirmPasswordText.equals("Confirm Password")){
            test.info("Filed name matched");
        }else{
            test.info("Filed name does not matched");
        }
    }


    //---verify confirm password placeholder---//
    public void verify_confirm_password_placeholder(){
        String ConfirmPasswordPlaceHolderText = ConfirmPasswordPlaceholder.getAttribute("value");
        if(ConfirmPasswordPlaceHolderText.equals("Confirm Password")){
            test.info("Place holder name matched");
        }else{
            test.info("Place holder name does not matched");
        }
    }

    //--- verify confirm password with blank space  ---//
    public void verify_confirm_password_with_blank_space() {
        ConfirmPassword.click();
        ConfirmPassword.sendKeys(" TEST@123 ");
        String ConfirmPasswordText = ConfirmPassword.getAttribute("value");
        Assert.assertNotEquals(ConfirmPasswordText," TEST@123 ");
    }


    //--- verify confirm password eye button ---//
    public void verify_confirm_password_eye_button(){
        ConfirmPassword.click();
        if(ConfirmPasswordEyeButton.isDisplayed()){
            test.info("Password eye button is displayed");
        }else {
            test.info("Password eye button does not displayed");
        }

    }

    //--- verify confirm password eye button in hide mode ---//
    public void verify_confirm_password_eye_hide_button(){
        if(ConfirmPasswordEyeHideButton.isDisplayed()){
            test.info("Password eye button is in hide mode");
        }else {
            test.info("Password eye button does not hide");
        }

    }

    //--- verify confirm password filed should be secure  ---//
    public void verify_confirm_password_filed_should_be_secure() {
        ConfirmPassword.click();
        ConfirmPassword.sendKeys("TEST@123");
        ConfirmPassword.sendKeys(Keys.TAB);
        String ConfirmPasswordText = ConfirmPassword.getAttribute("value");
        Assert.assertEquals(ConfirmPasswordText,"........");
    }

    //--- verify confirm password accept the emoji ---//
    public void verify_confirm_password_accept_the_emoji() throws InterruptedException {
        ConfirmPassword.click();
        CharSequence text = "'this my text \uD83E\uDD2F\uD83D\uDE21\uD83D\uDDE3\uD83D\uDC63' ";
        ConfirmPassword.sendKeys("\uD83D\uDE12");
        Thread.sleep(1000);
        String ConfirmPasswordText = ConfirmPassword.getAttribute("value");
        Assert.assertSame(ConfirmPasswordText,"");
    }


    //--- verify hand icon show hover over the eye icon for confirm password---//
    public void verify_hand_icon_hover_over_the_eye_icon_confirm_password() throws InterruptedException {
        String handicon = ConfirmPasswordEyeButton.getCssValue("cursor");
        Assert.assertEquals("handicon", "pointer");
    }


    //---verify new password filed text---//
    public void verify_new_password_filed_name(){
        String NewPasswordText = NewPasswordFiled.getAttribute("value");
        if(NewPasswordText.equals("New Password")){
            test.info("Filed name matched");
        }else{
            test.info("Filed name does not matched");
        }
    }


    //---verify new password placeholder---//
    public void verify_new_password_placeholder(){
        String NewPasswordPlaceHolderText = NewPasswordPlaceholder.getAttribute("value");
        if(NewPasswordPlaceHolderText.equals("New Password")){
            test.info("Place holder name matched");
        }else{
            test.info("Place holder name does not matched");
        }
    }

    //--- verify new password with blank space  ---//
    public void verify_new_password_with_blank_space() {
        NewPassword.click();
        NewPassword.sendKeys(" TEST@123 ");
        String NewPasswordText = NewPassword.getAttribute("value");
        Assert.assertNotEquals(NewPasswordText," TEST@123 ");
    }


    //--- verify new password eye button ---//
    public void verify_new_password_eye_button(){
        NewPassword.click();
        if(NewPasswordEyeButton.isDisplayed()){
            test.info("Password eye button is displayed");
        }else {
            test.info("Password eye button does not displayed");
        }

    }

    //--- verify new password eye button in hide mode ---//
    public void verify_new_password_eye_hide_button(){
        if(NewPasswordEyeHideButton.isDisplayed()){
            test.info("Password eye button is in hide mode");
        }else {
            test.info("Password eye button does not hide");
        }

    }

    //--- verify new password filed should be secure  ---//
    public void verify_new_password_filed_should_be_secure() {
        NewPassword.click();
        NewPassword.sendKeys("TEST@123");
        NewPassword.sendKeys(Keys.TAB);
        String NewPasswordText = NewPassword.getAttribute("value");
        Assert.assertEquals(NewPasswordText,"........");
    }

    //--- verify new password accept the emoji ---//
    public void verify_new_password_accept_the_emoji() throws InterruptedException {
        NewPassword.click();
        CharSequence text = "'this my text \uD83E\uDD2F\uD83D\uDE21\uD83D\uDDE3\uD83D\uDC63' ";
        NewPassword.sendKeys("\uD83D\uDE12");
        Thread.sleep(1000);
        String NewPasswordText = NewPassword.getAttribute("value");
        Assert.assertSame(NewPasswordText,"");
    }


    //--- verify hand icon show hover over the eye icon for new password---//
    public void verify_hand_icon_hover_over_the_eye_icon_new_password() throws InterruptedException {
        String handicon = NewPasswordEyeButton.getCssValue("cursor");
        Assert.assertEquals("handicon", "pointer");
    }

    //---verify current password filed text---//
    public void verify_current_password_filed_name(){
        String CurrentPasswordText = CurrentPasswordFiled.getAttribute("value");
        if(CurrentPasswordText.equals("Current Password")){
            test.info("Filed name matched");
        }else{
            test.info("Filed name does not matched");
        }
    }


    //---verify current password placeholder---//
    public void verify_current_password_placeholder(){
        String CurrentPasswordPlaceHolderText = CurrentPasswordPlaceholder.getAttribute("value");
        if(CurrentPasswordPlaceHolderText.equals("Current Password")){
            test.info("Place holder name matched");
        }else{
            test.info("Place holder name does not matched");
        }
    }

    //--- verify current password with blank space  ---//
    public void verify_current_password_with_blank_space() {
        CurrentPassword.click();
        CurrentPassword.sendKeys(" TEST@123 ");
        String CurrentPasswordText = CurrentPassword.getAttribute("value");
        Assert.assertNotEquals(CurrentPasswordText," TEST@123 ");
    }


    //--- verify current password eye button ---//
    public void verify_current_password_eye_button(){
        CurrentPassword.click();
        if(CurrentPasswordEyeButton.isDisplayed()){
            test.info("Password eye button is displayed");
        }else {
            test.info("Password eye button does not displayed");
        }

    }

    //--- verify current password eye button in hide mode ---//
    public void verify_current_password_eye_hide_button(){
        if(CurrentPasswordEyeHideButton.isDisplayed()){
            test.info("Password eye button is in hide mode");
        }else {
            test.info("Password eye button does not hide");
        }

    }

    //--- verify current password filed should be secure  ---//
    public void verify_current_password_filed_should_be_secure() {
        CurrentPassword.click();
        CurrentPassword.sendKeys("TEST@123");
        CurrentPassword.sendKeys(Keys.TAB);
        String CurrentPasswordText = CurrentPassword.getAttribute("value");
        Assert.assertEquals(CurrentPasswordText,"........");
    }

    //--- verify current password accept the emoji ---//
    public void verify_current_password_accept_the_emoji() throws InterruptedException {
        CurrentPassword.click();
        CharSequence text = "'this my text \uD83E\uDD2F\uD83D\uDE21\uD83D\uDDE3\uD83D\uDC63' ";
        CurrentPassword.sendKeys("\uD83D\uDE12");
        Thread.sleep(1000);
        String CurrentPasswordText = NewPassword.getAttribute("value");
        Assert.assertSame(CurrentPasswordText,"");
    }


    //--- verify hand icon show hover over the eye icon for current password---//
    public void verify_hand_icon_hover_over_the_eye_icon_current_password() throws InterruptedException {
        String handicon = CurrentPasswordEyeButton.getCssValue("cursor");
        Assert.assertEquals("handicon", "pointer");
    }

    //---verify old password filed text---//
    public void verify_old_password_filed_name(){
        String OldPasswordText = OldPasswordFiled.getAttribute("value");
        if(OldPasswordText.equals("Old Password")){
            test.info("Filed name matched");
        }else{
            test.info("Filed name does not matched");
        }
    }


    //---verify old password placeholder---//
    public void verify_old_password_placeholder(){
        String OldPasswordPlaceHolderText = OldPasswordPlaceholder.getAttribute("value");
        if(OldPasswordPlaceHolderText.equals("Old Password")){
            test.info("Place holder name matched");
        }else{
            test.info("Place holder name does not matched");
        }
    }

    //--- verify old password with blank space  ---//
    public void verify_old_password_with_blank_space() {
        OldPassword.click();
        OldPassword.sendKeys(" TEST@123 ");
        String OldPasswordText = OldPassword.getAttribute("value");
        Assert.assertNotEquals(OldPasswordText," TEST@123 ");
    }


    //--- verify old password eye button ---//
    public void verify_old_password_eye_button(){
        OldPassword.click();
        if(OldPasswordEyeButton.isDisplayed()){
            test.info("Password eye button is displayed");
        }else {
            test.info("Password eye button does not displayed");
        }

    }

    //--- verify old password eye button in hide mode ---//
    public void verify_old_password_eye_hide_button(){
        if(OldPasswordEyeHideButton.isDisplayed()){
            test.info("Password eye button is in hide mode");
        }else {
            test.info("Password eye button does not hide");
        }

    }

    //--- verify old password filed should be secure  ---//
    public void verify_old_password_filed_should_be_secure() {
        OldPassword.click();
        OldPassword.sendKeys("TEST@123");
        OldPassword.sendKeys(Keys.TAB);
        String OldPasswordText = OldPassword.getAttribute("value");
        Assert.assertEquals(OldPasswordText,"........");
    }

    //--- verify old password accept the emoji ---//
    public void verify_old_password_accept_the_emoji() throws InterruptedException {
        OldPassword.click();
        CharSequence text = "'this my text \uD83E\uDD2F\uD83D\uDE21\uD83D\uDDE3\uD83D\uDC63' ";
        OldPassword.sendKeys("\uD83D\uDE12");
        Thread.sleep(1000);
        String OldPasswordText = OldPassword.getAttribute("value");
        Assert.assertSame(OldPasswordText,"");
    }


    //--- verify hand icon show hover over the eye icon for old password---//
    public void verify_hand_icon_hover_over_the_eye_icon_old_password() throws InterruptedException {
        String handicon = OldPasswordEyeButton.getCssValue("cursor");
        Assert.assertEquals("handicon", "pointer");
    }
    public void verify_signup_with_valid_data() throws InterruptedException {
        SignUpPage sign_up = new SignUpPage(driver);
        SignInPage sign_in = new SignInPage(driver);
        RegisterPage register = new RegisterPage(driver);
        sign_up.click_on_sign_up_button();
        FirstName.sendKeys("Test");
        LastName.sendKeys("Test");
        Password.sendKeys("Test@123");
        sign_in.InputEmail.sendKeys("test"+ ThreadLocalRandom.current().nextInt()+"@gmail.com");
        Thread.sleep(3000);
        register.CalenderArrowButton.click();
        register.verify_birthdate_formate();
        sign_up.SignUpPageButton.click();
        Thread.sleep(10000);
        VerifyAccount.isDisplayed();

    }




}
